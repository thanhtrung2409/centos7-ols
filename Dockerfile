FROM centos:centos7.9.2009


RUN yum install -y wget \
    nano \
    htop \
    libpng-devel libmcrypt-devel \
    libxml2-devel openssl-devel curl-devel \
    epel-release \
    libzip-devel.x86_64 \
    supervisor

RUN yum groupinstall -y "Development Tools"

RUN rpm -Uvh http://rpms.litespeedtech.com/centos/litespeed-repo-1.1-1.el6.noarch.rpm
RUN yum install -y openlitespeed

RUN yum install -y lsphp56 lsphp56-common lsphp56-mysql lsphp56-gd lsphp56-process lsphp56-mbstring lsphp56-xml lsphp56-mcrypt lsphp56-pdo lsphp56-imap lsphp56-soap lsphp56-bcmath

RUN yum install -y lsphp74 lsphp74-common lsphp74-mysql lsphp74-gd lsphp74-process lsphp74-mbstring lsphp74-xml lsphp74-mcrypt lsphp74-pdo lsphp74-imap lsphp74-soap lsphp74-bcmath lsphp74-json lsphp74-devel lsphp74-mysqli


# INSTALL imagick
RUN yum install -y libjpeg icu libX11 libXext libXrender xorg-x11-fonts-Type1 xorg-x11-fonts-75dpi
RUN yum install -y libjpeg icu libX11 libXext libXrender xorg-x11-fonts-Type1 xorg-x11-fonts-75dpi

RUN yum update -y
RUN yum install ImageMagick ImageMagick-devel -y

ENV IMAGEMAGICK_VERSION="7.0.9-10"
ENV IMAGEMAGICK_LIB_RPM_URL="https://imagemagick.org/download/linux/CentOS/x86_64/ImageMagick-libs-${IMAGEMAGICK_VERSION}.x86_64.rpm"
ENV IMAGEMAGICK_RPM_URL="https://imagemagick.org/download/linux/CentOS/x86_64/ImageMagick-${IMAGEMAGICK_VERSION}.x86_64.rpm"

RUN export HTTP_CODE=$(curl -s -o /dev/null -w "%{http_code}" $IMAGEMAGICK_RPM_URL)
RUN if [ $HTTP_CODE != "200" ] ; then echo "$IMAGEMAGICK_RPM_URL does not exist" && exit 1 ; fi

RUN curl -s -S "https://download-ib01.fedoraproject.org/pub/epel/7/x86_64/Packages/l/libraqm-0.7.0-1.el7.x86_64.rpm" -o libraqm.rpm && \
    curl -s -S $IMAGEMAGICK_LIB_RPM_URL -o imagemagick-lib.rpm && \
    curl -s -S $IMAGEMAGICK_RPM_URL -o imagemagick.rpm

RUN yum -y update && \
    yum localinstall -y libraqm.rpm imagemagick*.rpm && \
    rm -f imagemagick*.rpm && \
    yum clean all



# INSTALL wkhtmltopdf
RUN wget https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox-0.12.6-1.centos7.x86_64.rpm

RUN rpm -Uvh wkhtmltox-0.12.6-1.centos7.x86_64.rpm

# INSTALL PHP74 extension zip
RUN rpm -e --nodeps libzip-devel  libzip
RUN wget https://nih.at/libzip/libzip-1.2.0.tar.gz && tar -zxvf libzip-1.2.0.tar.gz &&  cd libzip-1.2.0 && ./configure && make && make install && cp /usr/local/lib/libzip/include/zipconf.h /usr/local/include/zipconf.h
RUN echo "/usr/local/lib64" >> /etc/ld.so.conf
RUN echo "/usr/local/lib" >> /etc/ld.so.conf
RUN echo "/usr/lib64" >> /etc/ld.so.conf
RUN cd /usr/local/lsws/lsphp74/bin/ && wget http://pear.php.net/go-pear.phar && ./php go-pear.phar && ./pecl install zip
RUN cd /usr/local/lsws/lsphp74/etc/php.d && echo 'extension=zip.so' > 20-zip.ini


EXPOSE 8088
EXPOSE 7080
EXPOSE 5353

#start openlitespeed server
ENTRYPOINT "/init.sh/start-lsws.sh" && /bin/bash